import React, {Component} from 'react';
import CurrencyInput from "./CurrencyInput";

class CurrencyTranslator extends Component {
  constructor(props){
    super(props);
    this.state={
      amountUSD:'',
      amountCNY:''
    };
    this.handleAmountUSD=this.handleAmountUSD.bind(this);
    this.handleAmountCNY=this.handleAmountCNY.bind(this);
  }

  handleAmountUSD(amount){
    this.setState({amountUSD:amount/6,amountCNY:amount});
    setTimeout(()=>{
      console.log(amount);
      console.log(this.state.amountUSD+"NEW_USD");
      console.log(this.state.amountCNY+"NEW_CNY");
    })
  }

  handleAmountCNY(amount){
    this.setState({amountCNY:amount*6,amountUSD:amount},()=>{
      console.log('callback',this.state);
    });
    console.log('...',this.state);
  }

  render() {
    return (
      <div>
        <CurrencyInput currency="USD" amount={this.state.amountUSD} changeAmount={this.handleAmountCNY}/>
        <CurrencyInput currency="CNY" amount={this.state.amountCNY} changeAmount={this.handleAmountUSD}/>
      </div>
    )
  }
}

export default CurrencyTranslator;